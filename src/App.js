import Navbar from "./components/Navbar/Navbar";
import NavbarFunctional from "./components/Navbar/NavbarFunctional";
import Home from "./page/Home";

function App() {
	return (
		<div className="App">
			{/* Header */}
			<NavbarFunctional />
			<br />

			{/* Content */}
			<Home />

			{/* Footer */}
		</div>
	);
}

export default App;
