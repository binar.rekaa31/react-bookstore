import React, { Component } from 'react';
import { connect } from 'react-redux';
import { removeFavoriteBook } from '../../store/actions';

class Navbar extends Component {
    render() {
        return (
            <>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col" width="100%">Title</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.props.list_book.map((item,index) => 
                                            <tr>
                                                <th scope="row">{index + 1}</th>
                                                <td>{item.title}</td>
                                                <td><button className='btn btn-danger' onClick={() => this.props.dispatch(removeFavoriteBook(item.id))}>Remove</button></td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="navbar navbar-light bg-light">
                    <div class="container-fluid">
                        <a class="navbar-brand" href="#">Navbar</a>
                        <button className='btn btn-primary position-relative' data-bs-toggle="modal" data-bs-target="#exampleModal">
                            My Favorite Books
                            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                {this.props.list_book.length}
                                <span class="visually-hidden">unread messages</span>
                            </span>
                        </button>
                    </div>
                </nav>
            </>
        );
    }
}

function store(state){
    return{
        list_book : state.book
    }
}

export default connect(store)(Navbar);