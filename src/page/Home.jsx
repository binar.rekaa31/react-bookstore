import React, { Component } from 'react'
import Card from '../components/Card/Card'

class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            list_books: [
                {
                    id: 1,
                    title: "Books 1",
                    content : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga, ratione possimus architecto fugiat eius quidem porro ipsa ab nobis iure nihil earum velit dolor et quia sapiente libero deleniti consequatur."
                },
                {
                    id: 2,
                    title: "Books 2",
                    content : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga, ratione possimus architecto fugiat eius quidem porro ipsa ab nobis iure nihil earum velit dolor et quia sapiente libero deleniti consequatur."
                }
            ]
        }
    }


    render() {
        return (
            <div className='container-fluid'>
                <div className='row'>
                    {this.state.list_books.map((item) =>
                        <div className='col-2'>
                            <Card 
                                id={item.id} 
                                title={item.title} 
                                content={item.content}
                            />
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

export default Home