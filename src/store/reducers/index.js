// Import Module Combine Reducers
import { combineReducers } from "redux";

// Import Reducers
import booksReducers from "./BooksReducers";
import themeWebReducers from "./ThemeReducers";

const reducers = combineReducers({
    book : booksReducers,
    theme : themeWebReducers
})

export default reducers