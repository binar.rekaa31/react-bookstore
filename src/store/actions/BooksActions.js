export const ADD_FAVORITE_BOOK = "ADD_FAVORITE_BOOK"
export const REMOVE_FAVORITE_BOOK = "REMOVE_FAVORITE_BOOK"

export const addFavoriteBook = ({id, title}) => ({
    type : ADD_FAVORITE_BOOK,
    payload : {
        id, 
        title
    }
})

export const removeFavoriteBook = (id) => ({
    type : REMOVE_FAVORITE_BOOK,
    payload : {
        id
    }
})